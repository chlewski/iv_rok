#include<iostream>
#include<map>

double fun(double x) {
	return x + pow(x, -2);
}
int n = 0;
void recu(double a, double b, double funxm)
{
	n++;
	double xm = (a + b) / 2.0;
	double x1 = (xm + a) / 2.0;
	double x2 = (xm + b) / 2.0;

	double funx1 = fun(x1);
	//double funxm = fun(xm);
	double funx2 = fun(x2);

	if ((a + b) / (pow(2, n)) > 0.01)
	{

		if (funx1 < funxm)
		{
			recu(a, xm, fun((a + xm) / 2.0));
		}
		else if (funxm > funx2)
		{
			recu(xm, b, fun((xm + b) / 2.0));
		}
		else
		{
			recu(x1, x2, fun((x1 + x2) / 2.0));
		}
	}
	else
	{
		std::cout << "a  = " << a << std::endl;
		std::cout << "xm = " << xm << std::endl;
		std::cout << "fun(xm) = " << fun(xm) << std::endl;
		std::cout << "b = " << b << std::endl;
		std::cout << "ilosc wywolan n = " << n << std::endl;
	}
}


int main() {
	std::map<double, std::pair<double, double>> valueMap;

	for (double i = 1.0; i < 2.0; i = i + 0.01) {
		if (fun(i - 0.01) > fun(i) && fun(i) > fun(i + 0.01)) {
			std::pair<double, double> pairValue = std::make_pair(i + 0.01, i - 0.01);
			double x0 = fun(i);
			valueMap[x0] = pairValue;
		}
	}

	std::cout << "x0" << "\t" << "f(i+0.01)" << "\t" << "f(i-0.01)" << "\n";
	for (std::map<double, std::pair<double, double> >::const_iterator it = valueMap.begin();
		it != valueMap.end(); ++it)
	{
		std::cout << it->first << "\t" << it->second.first << "\t" << it->second.second << "\n";
	}

	//ZAD.2 - podzial polowkowy
	std::cout << "\n\n\n\n" << "zad2\n";
	recu(1, 2, fun((1 + 2) / 2.0));

	system("pause");
	return 0;
}